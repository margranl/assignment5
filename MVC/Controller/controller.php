<?php
include_once("model/model.php");  
include_once("SkierLogs.xml");

  
class Controller { 
    public $model; 
	
  
    public function __construct()    
    {    
		$this->model = new Model();  
    }   
      
	public function processDocument($docUrl)  
    {  
		$doc = new DOMDocument();
		if (!$doc->load($docUrl)) {
        $title = 'Operasjon feilet';
        $body = '<p>Klarte ikke å laste XML-fil</p>';
		} else {
 
        $xpath = new DOMXpath($doc);
        $elements = $xpath->query("/SkierLogs/*");
		processDocument('SkierLogs.xml');
		}
          
    } 
	
	
	
 
	} 
?>