-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 02. Nov, 2017 13:57 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oblig5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `clubid` int(11) NOT NULL,
  `clubname` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `county` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `clubskierseason`
--

CREATE TABLE `clubskierseason` (
  `season` int(4) NOT NULL,
  `username` varchar(250) NOT NULL,
  `totalDistance` int(255) DEFAULT NULL,
  `clubid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `entry`
--

CREATE TABLE `entry` (
  `date` date NOT NULL,
  `area` varchar(250) NOT NULL,
  `distance` int(100) DEFAULT NULL,
  `season` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(250) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `yearOfBirth` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`clubid`);

--
-- Indexes for table `clubskierseason`
--
ALTER TABLE `clubskierseason`
  ADD PRIMARY KEY (`season`),
  ADD KEY `clubid` (`clubid`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `entry`
--
ALTER TABLE `entry`
  ADD KEY `season` (`season`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `clubskierseason`
--
ALTER TABLE `clubskierseason`
  ADD CONSTRAINT `clubskierseason_ibfk_1` FOREIGN KEY (`clubid`) REFERENCES `club` (`clubid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `clubskierseason_ibfk_2` FOREIGN KEY (`username`) REFERENCES `skier` (`userName`) ON UPDATE CASCADE;

--
-- Begrensninger for tabell `entry`
--
ALTER TABLE `entry`
  ADD CONSTRAINT `entry_ibfk_1` FOREIGN KEY (`season`) REFERENCES `clubskierseason` (`season`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
